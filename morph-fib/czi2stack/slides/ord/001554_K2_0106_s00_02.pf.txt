//// overwrite defaults from parameterFile.txt for specific image (pair)
//(MaximumStepLength 0.0) // to avoid any change during 0th reg. iteration
(MaximumNumberOfIterations 0 100) // only one (0th) iteration
(NumberOfResolutions 2) // different pyramid levels
(ImagePyramidSchedule  32 32  16 16) // use different resolutions
